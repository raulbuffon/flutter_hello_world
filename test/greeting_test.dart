import 'package:flutter_test/flutter_test.dart';
import 'package:hello_world/greeting.dart';

void main() {
  test('Hello World Salutation', ()
  {expect(Greeting().salutation(), 'Hello World');
  });
}