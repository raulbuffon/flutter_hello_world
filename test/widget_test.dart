import 'package:flutter_test/flutter_test.dart';
import 'package:hello_world/helloworldapp.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {

    await tester.pumpWidget(HelloWorldApp());

    expect(find.text('Hello World'), findsOneWidget);
  });
}